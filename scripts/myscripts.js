/*var url = 'images/new/'

function newImage(obj, cmll) {
	var x = url.search('new/')
	var alg = obj.innerHTML;
	var turn = alg.search(' ')

	turn = alg.substr(0, turn)
	if (turn == '(U)') {
		turn = 'u';
	} else if (turn == "(U')") {
		turn = 'u-';
	} else {
		turn = 'u2';
	}

	var newUrl = "url('" + url.substr(0, x) + 'hover/' + cmll + turn +".gif')";
	document.getElementById(cmll).style.backgroundImage = newUrl;
};

function oldImage(cmll) {
	var x = url.search('hover/');
	var newUrl = "url('" + url + cmll + ".gif')";
	document.getElementById(cmll).style.backgroundImage = newUrl;
};

var pre = 'https://alg.cubing.net/?type=alg&view=playback&alg='
function populateHref(obj) {
	var alg = obj.innerHTML;
	alg = alg.replace(/ /g, '_').replace(/'/g, '-')
	obj.href = pre + alg;
};*/

function rotator(obj, cmll, over) {
	var alg = obj.innerHTML;
	var turn = alg.search(' ')

	turn = alg.substr(0, turn)
	if (turn == '(U)') {
		turn = over ? ['rotate(90deg)', '100ms']
								: ['rotate(0)', '100ms'];
	} else if (turn == "(U')") {
		turn = over ? ['rotate(-90deg)', '100ms']
								: ['rotate(0)', '100ms'];
	} else {
		turn = over ? ['rotate(180deg)', '200ms']
								: ['rotate(0)', '200ms'];
	}

	document.getElementById(cmll).style.transition = turn[1];
	document.getElementById(cmll).style.transform = turn[0];
	document.getElementById(cmll).style.zIndex = '0';
}

function linker() {
	var y = document.getElementsByClassName('alg');
	var pref = 'https://alg.cubing.net/?type=alg&view=playback&alg=';
	var algs = [];
	var refs = [];
	
	var i;
	for(i = 0; i < y.length; i++) {
		algs[i] = y[i].innerHTML.replace(/ /g, '_').replace(/'/g, '-');
		refs[i] = pref + algs[i];
		y[i].href = refs[i];
	}
};